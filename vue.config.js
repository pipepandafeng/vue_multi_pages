module.exports = {
  pages: {
    index: {
      entry: "src/main.js",
    },
    sub_pro1: {
      entry: "src/sub_pro1/main.js",
      filename: "sub_pro1/index.html",
    },
    sub_pro2: {
      entry: "src/sub_pro2/main.js",
      filename: "sub_pro2/index.html",
    },
  },
};
